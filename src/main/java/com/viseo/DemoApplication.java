package com.viseo;

import com.viseo.domain.players.CDPlayer;
import com.viseo.domain.players.DVDPlayer;
import com.viseo.domain.players.MediaPlayer;
import com.viseo.repositories.JdbcCDRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


@SpringBootApplication
public class DemoApplication {
	public final static Logger LOG = LoggerFactory.getLogger(DemoApplication.class);

	public static void main(String[] args) {
		LOG.info("Starting Application");
        ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);
        MediaPlayer cdPlayer = context.getBean(CDPlayer.class);
        cdPlayer.play();
        MediaPlayer dvdPlayer = context.getBean(DVDPlayer.class);
        dvdPlayer.play();

        JdbcCDRepository repo = context.getBean(JdbcCDRepository.class);
        repo.getAllCDs().forEach(System.out::println);
    }
}
