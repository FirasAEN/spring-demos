package com.viseo.domain.media;

import com.viseo.domain.players.CDPlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Created by FAB3659 on 07-May-18.
 */
@Component
@Qualifier("DVD")
public class DVD implements MediaObject {
    final static Logger LOG = LoggerFactory.getLogger(CDPlayer.class);
    private Environment env;

    private String name;

    public DVD(){}

    @Autowired
    public DVD(Environment env){
        this.env = env;
        this.name = env.getProperty("dvd.name");
    }

    @Override
    public void play() {
        LOG.debug("Playing DVD");
    }

    @Override
    public String toString() {
        return "DVD{" +
                "name= " + name + "}";
    }
}
