package com.viseo.domain.media;

/**
 * Created by FAB3659 on 07-May-18.
 */
public interface MediaObject {
    void play();
}
