package com.viseo.domain.media;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Created by FAB3659 on 07-May-18.
 */
@Component
@Qualifier("CD")
public class CD implements MediaObject{
    final static Logger LOG = LoggerFactory.getLogger(CD.class);

    @Value("${cd.name}")
    private String name;

    public CD(){

    }

    public CD(String cdName){
        this.name = cdName;
    }

    @Override
    public void play() {
        LOG.info("Playing CD");
        System.out.println("Playing CD");
    }

    @Override
    public String toString() {
        return "CD{" +
                "name='" + name + '\'' +
                '}';
    }

    public void setName(String name){
        this.name = name;
    }
}
