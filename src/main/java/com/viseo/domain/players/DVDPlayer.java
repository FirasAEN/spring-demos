package com.viseo.domain.players;

import com.viseo.domain.media.MediaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by FAB3659 on 07-May-18.
 */
@Component
@Qualifier("DVD_PLAYER")
public class DVDPlayer implements MediaPlayer {
    final static Logger LOG = LoggerFactory.getLogger(CDPlayer.class);
    private final MediaObject dvd;

    @Autowired
    public DVDPlayer(@Qualifier("DVD") MediaObject dvd){
        this.dvd = dvd;
    }

    @Override
    public void play() {
        LOG.info("Dvd player started");
        dvd.play();
        LOG.info(dvd.toString());
    }

    @Override
    public MediaObject getMediaObject() {
        return this.dvd;
    }
}
