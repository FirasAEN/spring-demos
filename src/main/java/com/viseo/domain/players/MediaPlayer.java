package com.viseo.domain.players;

import com.viseo.domain.media.MediaObject;

/**
 * Created by FAB3659 on 07-May-18.
 */
public interface MediaPlayer {

    void play();

    MediaObject getMediaObject();
}
