package com.viseo.domain.players;

import com.viseo.domain.media.MediaObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * Created by FAB3659 on 07-May-18.
 */
@Component
@Qualifier("CD_PLAYER")
public class CDPlayer implements MediaPlayer {
    final static Logger LOG = LoggerFactory.getLogger(CDPlayer.class);
    private final MediaObject cd;

    @Autowired
    public CDPlayer(@Qualifier("CD") MediaObject cd){
        this.cd = cd;
    }

    @Override
    public void play() {
        LOG.info("Cd player started");
        cd.play();
        LOG.info(cd.toString());
    }

    public MediaObject getMediaObject() {
        return this.cd;
    }
}
