package com.viseo.repositories;

import com.viseo.domain.media.CD;

import java.util.List;

/**
 * Created by Firas on 5/10/2018.
 */
public interface CDRepository {
    List<CD> getAllCDs();
}
