package com.viseo.repositories;

import com.viseo.domain.media.CD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Firas on 5/10/2018.
 */
@Repository
public class JdbcCDRepository implements CDRepository {
    private static final Logger log = LoggerFactory.getLogger(JdbcCDRepository.class);

    @Autowired
    private JdbcOperations jdbc;


    @Override
    public List<CD> getAllCDs() {
        log.warn("Get all CDs ...");
        String query = "SELECT * FROM cd";
        List<CD> lst = jdbc.query(query, new CDRowMapper());
        return lst;
    }


    private final static class CDRowMapper implements RowMapper<CD>{

        @Override
        public CD mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            String cdName = resultSet.getString("label");
            return new CD(cdName);
        }
    }
}
