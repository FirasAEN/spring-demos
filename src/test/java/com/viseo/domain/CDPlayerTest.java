package com.viseo.domain;

import com.viseo.config.AppConfig;
import com.viseo.domain.players.MediaPlayer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

/**
 * Created by FAB3659 on 07-May-18.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class CDPlayerTest {
    final static Logger LOG = LoggerFactory.getLogger(CDPlayerTest.class);

    @Autowired
    @Qualifier("CD_PLAYER")
    MediaPlayer cdPlayer;

    @Test
    public void testPlay() throws Exception {
        assertNotNull(cdPlayer);
        LOG.debug("cd player successfully injected");
    }

    @Test
    public void testPlayWithCD() throws Exception {
        assertNotNull(cdPlayer.getMediaObject());
        LOG.debug("cd player dependency of cd ({}) successfully injected", cdPlayer.getMediaObject().toString());
    }
}