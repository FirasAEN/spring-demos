package com.viseo.repositories;

import com.viseo.config.AppConfig;
import com.viseo.domain.media.CD;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by Firas on 5/11/2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
public class JdbcCDRepositoryTest {

    @Autowired
    JdbcCDRepository repo;

    @Test
    public void testGetCDs(){
        List<CD> allCDs = repo.getAllCDs();
        allCDs.forEach(System.out::println);
    }
}